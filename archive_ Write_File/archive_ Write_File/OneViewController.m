//
//  OneViewController.m
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//

#import "OneViewController.h"
#import "USERINFOOne.h"
#import "USERINFOManage.h"
@interface OneViewController ()

@end

@implementation OneViewController
-(void)viewWillAppear:(BOOL)animated
{    self.navigationController.navigationBarHidden = YES;

    }
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    
        // Do any additional setup after loading the view, typically from a nib.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self.view endEditing:YES];
}
-(IBAction)saveUserInfo:(id)sender
{
    
    
    USERINFOOne * userOne  = [[USERINFOOne alloc] init];
    userOne.userName = [NSString stringWithFormat:@"%@",self.textUserName.text];
    
    userOne.userAge = [NSString stringWithFormat:@"%@",self.textUserAge.text];
    userOne.userBrithday = [NSString stringWithFormat:@"%@",self.textUserBrithday.text];
    userOne.userSex = [NSString stringWithFormat:@"%@",self.textUserSex.text];
    userOne.userRemark = [NSString stringWithFormat:@"%@",self.textUserRemark.text];
    
    BOOL isSucess = [[USERINFOManage shareUserManage] saveInsetUserInfo:userOne];
    if (isSucess) {
        
        self.textUserName.text = @"";
        self.textUserRemark.text = @"";
        self.textUserSex.text = @"";
        self.textUserBrithday.text = @"";
        self.textUserAge.text = @"";
        NSLog(@"%hhd",isSucess);
        NSLog(@"成功写入用户数据");
        
        
    }
    
    
    
}

-(IBAction)readUserInfo:(id)sender
{
    
    USERINFOOne * readUserInfo = [[USERINFOManage shareUserManage] readUserInfo];
    
    
    self.textUserName.text = readUserInfo.userName;
    self.textUserRemark.text = readUserInfo.userRemark;
    self.textUserSex.text = readUserInfo.userSex;
    self.textUserBrithday.text = readUserInfo.userBrithday;
    self.textUserAge.text = readUserInfo.userAge;
    
        //    [self performSegueWithIdentifier:@"pushOneViewController" sender:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
