//
//  USERINFO.m
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//

#import "USERINFOOne.h"
#import "ReferenceMacro.h"
@implementation USERINFOOne
//-(instancetype)init
//{
//    self = [super init];
//
//    if (self) {
//    }
//    return self;
//}
//
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userName forKey:NSStringFromSelector(@selector(userName))];
    [aCoder encodeObject:self.userAge forKey:Key_userAge];
    [aCoder encodeObject:self.userBrithday forKey:Key_userBrithday];
    [aCoder encodeObject:self.userSex forKey:Key_userSex];
    [aCoder encodeObject:self.userRemark forKey:Key_userRemark];


}
//
-(id)initWithCoder:(NSCoder *)aDecoder
{
    
    self.userName =  [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(userName))];
    self.userAge =  [aDecoder decodeObjectForKey:Key_userAge];
    self.userBrithday =  [aDecoder decodeObjectForKey:Key_userBrithday];
    self.userSex =  [aDecoder decodeObjectForKey:Key_userSex];
    self.userRemark = [aDecoder decodeObjectForKey:Key_userRemark];
    return self;
}

    //如果添加了 NSCopying  如果添加了这个协议必须实现 -> - (id)copyWithZone:(NSZone *)zone
/*
- (id)copyWithZone:(NSZone *)zone {
    
    USERINFOOne * userInfoCopy = [[[self class]allocWithZone:zone]init];
    
    userInfoCopy.userName = [self.userName copyWithZone:zone];
       return userInfoCopy;
}
*/

@end
