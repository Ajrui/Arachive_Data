//
//  USERINFO.h
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USERINFOOne : NSObject<NSCoding>
/**
 *  用户名
 *  userName
 **/
@property (strong,nonatomic) NSString * userName;
/**
 *  用户年龄
 *  userAge
 **/
@property (strong,nonatomic) NSString * userAge;
/**
 *  用户生日
 * userBrithday
 **/
@property (strong,nonatomic) NSString * userBrithday;
/**
 *  用户性别
 *  userSex
 **/
@property (strong,nonatomic) NSString * userSex;
/**
 *  用户备注限制200个子
 *  userRemark
 **/
@property (strong,nonatomic) NSString * userRemark;

@end
