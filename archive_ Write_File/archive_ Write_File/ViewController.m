//
//  ViewController.m
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//

#import "ViewController.h"
#import "USERINFOOne.h"
#import "USERINFOManage.h"
@interface ViewController ()

@end

@implementation ViewController
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;

}
-(void)viewDidDisappear:(BOOL)animated
{

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self.view endEditing:YES];
}
-(IBAction)saveUserInfo:(id)sender
{
    
    
    USERINFOOne * userOne  = [[USERINFOOne alloc] init];
    userOne.userName = [NSString stringWithFormat:@"%@",self.textUserName.text];
    
    userOne.userAge = [NSString stringWithFormat:@"%@",self.textUserAge.text];
    userOne.userBrithday = [NSString stringWithFormat:@"%@",self.textUserBrithday.text];
    userOne.userSex = [NSString stringWithFormat:@"%@",self.textUserSex.text];
    userOne.userRemark = [NSString stringWithFormat:@"%@",self.textUserRemark.text];

    BOOL isSucess = [[USERINFOManage shareUserManage] saveInsetUserInfo:userOne];
    if (isSucess) {
        
        self.textUserName.text = @"";
        self.textUserRemark.text = @"";
        self.textUserSex.text = @"";
        self.textUserBrithday.text = @"";
        self.textUserAge.text = @"";
        NSLog(@"%hhd",isSucess);
        NSLog(@"成功写入用户数据");
        

    }
    
    
    
}

-(IBAction)readUserInfo:(id)sender
{
    
    USERINFOOne * readUserInfo = [[USERINFOManage shareUserManage] readUserInfo];
    
    
    self.textUserName.text = readUserInfo.userName;
    self.textUserRemark.text = readUserInfo.userRemark;
    self.textUserSex.text = readUserInfo.userSex;
    self.textUserBrithday.text = readUserInfo.userBrithday;
    self.textUserAge.text = readUserInfo.userAge;
    
//    [self performSegueWithIdentifier:@"pushOneViewController" sender:nil];

}



-(void)pushOne:(id)sender
{

    
    
//    UIStoryboard *schoolStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *detailVC = [schoolStoryBoard instantiateViewControllerWithIdentifier:@"pushOneViewControllerIdentifier"];
//    [self.navigationController pushViewController:detailVC animated:YES];
    
    
}
//-(UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
//{
//
//
//}
//-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender
//{
//    
//    
//
//
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
