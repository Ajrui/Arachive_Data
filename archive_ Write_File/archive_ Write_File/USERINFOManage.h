//
//  USERINFOManage.h
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USERINFOOne.h"
@interface USERINFOManage : NSObject
{
    BOOL isLoginYes;
}
+(USERINFOManage *)shareUserManage;
    //得到文件路径
-(NSString *) documentsFilePath:(NSString *) fileName;
 ///归档
-(BOOL)saveInsetUserInfo:(USERINFOOne *)USER;
-(BOOL)updateUserInfo:(USERINFOOne * )UpdateUSER;


-(BOOL)deleteUserInfo;

    ///读档
-(USERINFOOne *)readUserInfo;
    ///是否已登陆
-(BOOL)isLogin;
@end
