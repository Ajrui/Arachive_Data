//
//  main.m
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
