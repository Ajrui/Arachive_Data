//
//  OneViewController.h
//  archive_ Write_File
//
//  Created by shaorui on 15/9/2.
//  Copyright (c) 2015年 archive_ Write_File. All rights reserved.
//


#import <UIKit/UIKit.h>
@class USERINFOOne;
@interface OneViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *textUserName;

@property (strong, nonatomic) IBOutlet UITextField *textUserAge;
@property (strong, nonatomic) IBOutlet UITextField *textUserSex;
@property (strong, nonatomic) IBOutlet UITextField *textUserRemark;
@property (strong, nonatomic) IBOutlet UITextField *textUserBrithday;

    //解档读取
-(IBAction)readUserInfo:(id)sender;
    //归档保存
-(IBAction)saveUserInfo:(id)sender;

@end

