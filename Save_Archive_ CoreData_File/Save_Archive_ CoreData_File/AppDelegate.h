//
//  AppDelegate.h
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/5.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

    //操作实际内容（操作持久层）
//NSManagedObjectContext（被管理的数据上下文）
//作用：插入数据，查询数据，删除数据
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
/*NSManagedObjectModel（被管理的数据模型）
 
 数据库所有表格或数据结构，包含各实体的定义信息
 
 作用：添加实体的属性，建立属性之间的关系
 
 操作方法：视图编辑器，或代码
 */
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;

/*
NSPersistentStoreCoordinator（持久化存储助理）
 
 相当于数据库的连接器
 
 作用：设置数据存储的名字，位置，存储方式，和存储时机
 */

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
    //NSManagedObject（被管理的数据记录）//相当于数据库中的表格记录


@property (strong,nonatomic) NSManagedObject * managedObject;
    //NSFetchRequest（获取数据的请求）//相当于查询语句
@property (strong,nonatomic) NSFetchRequest * fetchRequest;
    //NSEntityDescription（实体结构）相当于表格结构
@property (strong,nonatomic) NSEntityDescription * entityDescription;
//后缀为.xcdatamodeld的包
//
//里面是.xcdatamodel文件，用数据模型编辑器编辑



- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

