//
//  USERINFO.h
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/8.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class USERINFODTEAIL;

@interface USERINFO : NSManagedObject

@property (nonatomic, retain) NSString * userAge;
@property (nonatomic, retain) NSString * userBrithday;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * userRemark;
@property (nonatomic, retain) NSString * userSex;
@property (nonatomic, retain) USERINFODTEAIL *toUserDetail;
+(USERINFO *)shareUserInfo;
+(USERINFO *)initWithContext:(NSManagedObjectContext *)context isTemp:(BOOL)isTemp
;


@end
