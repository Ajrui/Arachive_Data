//
//  USERINFO.m
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/8.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import "USERINFO.h"
#import "USERINFODTEAIL.h"


@implementation USERINFO

@dynamic userAge;
@dynamic userBrithday;
@dynamic userName;
@dynamic userRemark;
@dynamic userSex;
@dynamic toUserDetail;
static USERINFO * userInfo = nil;


+(USERINFO *)shareUserInfo
{
    static dispatch_once_t  predicate;
    dispatch_once(&predicate, ^{
        userInfo = [[USERINFO alloc] init];
        
    });
    
    return userInfo;
}

+(USERINFO *)initWithContext:(NSManagedObjectContext *)context isTemp:(BOOL)isTemp
{
    USERINFO *modelObject = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"USERINFO" inManagedObjectContext:context];
    if (isTemp) {
        
        modelObject = [[USERINFO alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }else
        {
        modelObject = [[USERINFO alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
        }
    return modelObject;
}
@end
