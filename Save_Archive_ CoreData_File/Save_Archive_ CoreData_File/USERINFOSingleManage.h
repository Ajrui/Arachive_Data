//
//  USERINFOSingleManage.h
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/5.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import <Foundation/Foundation.h>
@class USERINFO;
@interface USERINFOSingleManage : NSObject

+(USERINFOSingleManage *)shareUSERINFOSingleManage;
@property (strong,nonatomic) USERINFO * USER;

@end
