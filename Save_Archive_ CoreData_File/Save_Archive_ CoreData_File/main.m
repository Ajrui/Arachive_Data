//
//  main.m
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/5.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
