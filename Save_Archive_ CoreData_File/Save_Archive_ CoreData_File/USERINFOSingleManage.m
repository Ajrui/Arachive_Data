//
//  USERINFOSingleManage.m
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/5.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import "USERINFOSingleManage.h"

static USERINFOSingleManage * userSingleManage = nil;

@implementation USERINFOSingleManage



+(USERINFOSingleManage *)shareUSERINFOSingleManage
{
    
    static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
        userSingleManage = [[USERINFOSingleManage alloc] init];
    });
    return userSingleManage;
    
}



@end
