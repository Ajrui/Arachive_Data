//
//  USERINFODTEAIL.h
//  Save_Archive_ CoreData_File
//
//  Created by shaorui on 15/9/8.
//  Copyright (c) 2015年 Save_Archive_ CoreData_File. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class USERINFO;

@interface USERINFODTEAIL : NSManagedObject

@property (nonatomic, retain) NSString * userDepartment;
@property (nonatomic, retain) NSString * userLike;
@property (nonatomic, retain) NSString * userNumber;
@property (nonatomic, retain) USERINFO *backUSERINFO;

@end
